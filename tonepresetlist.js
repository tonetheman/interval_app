Vue.component("tone-addpreset", {
    methods : {
        addNewPreset() {
            console.log("adding new preset");
            // need to hide the preset editor
            // need to show the toneeditor element
        }
    },
    template : `
    <img src="plus.svg" width="32" height="32"
    v-on:click="addNewPreset()">
    `
});

Vue.component("presetlist",{
    mounted() {
        console.log("presetlist mounted now",this);
    },
    props : {
        "presets" : Array
    },
    template : `
    <div>
    your mother is a preset list
    <ul id="presetul">
    <li v-for="(p,index) in presets"> {{index}} {{p}} </li>
    </ul>
    </div>
    `
});

Vue.component("loading",{
    template :`<div>loading...</div>`
});

Vue.component("tone-presetlist", {
    methods : {
    },
    data : function() {
        return {
            loading : true,
            presets: []
        };
    },
    created : function() {
        //console.log("create is called");
    },
    mounted : function() {
        localforage.keys().then((keys) => {

            console.log("in the callback here is keys:",keys);
            console.log("added to keys",keys);
            this.presets = keys;
            if (keys.length==0) {
                this.presets.push("fake preset")
            }

            this.loading = false;
        });

    },
    template : `
    <div>
    <h3>your presets</h3>
    <loading v-if="loading"></loading>
    <presetlist v-if="!loading" v-bind:presets="presets"></presetlist>
    <tone-addpreset></tone-addpreset>
    </div>
    `
});
