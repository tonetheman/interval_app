// COUNTER
Vue.component("tone-counter",{
    // define the initial values here
    // title will never change though
    // value is read in from the parent
    // the child can then mutate
    props : ["title","defval"],
    data : function() {
        return {
            // child takes defval from parent
            // then we mutate all to hell
            value : this.defval
        };
    },
    watch : {
        value : {
            handler : function() {
                // this is funny i could emit back to parent...
                // where is parent saving the data then?
                // prob somewhere the child could see in the first
                // place!!!!
                console.log("watch is called!!!");
                
                // tell somebody
                // this is emitting i can see it in devtools (vue)
                // but parent is not getting it
                this.$emit("tonechanged", { 
                    title : this.title, value : this.value
                    });
            }
        }
    },
    template : `<div>
    <div>{{title}}</div>
    <button v-on:click="value--">-</button>
    <span>{{ value }}</span>
    <button v-on:click="value++">+</button>
    </div>
    `
});

