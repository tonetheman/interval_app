// SAVE
Vue.component("tone-save", {
    methods : {
        saveAll() {
            console.log("save all called on instance");
            this.$emit("tonesavedata");
        }
    },
    template : `
    <img src="save.svg" width="32" height="32" v-on:click="saveAll()">
    `
});
