

let timer = null;
let sm = null;

const CUTOFF = 0;

class StateMachine {
	// start here is in milliseconds
	constructor() {
		this.state = "warmup";
	}

	start(opts) {
		// start is the second we started
		this.start = Math.floor(Date.now()/1000);
		console.log("this.start",this.start);

		let calc_time = this.start; // init value

		this.data = [];
		calc_time += opts.warmup_seconds;
		let tmp = {state: "warmup", threshold : calc_time};
		this.data.push(tmp);

		for (let i=0;i<opts.work_count;i++) {
			calc_time += opts.work_seconds;
			tmp = {state : `work${i+1}`, threshold : calc_time};
			this.data.push(tmp);
	
			calc_time += opts.rest_seconds;
			tmp  = { state : `rest${i+1}`, threshold : calc_time };
			this.data.push(tmp);	
		}

		// cooldown
		calc_time += opts.cooldown_seconds;
		tmp = { state : "cooldown", threshold : calc_time };
		this.data.push(tmp);
		
		calc_time += opts.cooldown_seconds;
		tmp = { state : "fin", threshold : calc_time};
		this.data.push(tmp);

		for (let i=0;i<this.data.length;i++) {
			console.log(this.data[i]);
		}
	
	}
	
	update(dt) {
		let current_second = Math.floor(dt/1000);

		let current_state = null;

		for (let i=0;i<this.data.length;i++) {
			let d = this.data[i];
			if (current_second<d.threshold) {
				this.state = d.state;
				// this is important find the 1st state
				// and stops looking
				break;
			}
		}

	}

	stop() {
		// nothing yet
	}
}


Vue.component("tone-timer",{
    data : function() {
        return {
            phase : "",
            counter : 60
        }
    },
    template :`
    <div> 
        <div><h3>countdown: {{counter}}</h3></div>
        <div>current phase: {{phase}}</div>
    <button v-on:click="stopcurrenttimer">stop current timer</button>
    </div>
    `,
    mounted() {
        sm = new StateMachine();
        let opts = {
            warmup_seconds : 10,
            work_seconds : 10,
            rest_seconds : 10,
            work_count : 1,
            cooldown_seconds : 10.
        };
        sm.start(opts);
        console.log("component mounted lets start");
        this.phase = sm.state;
        this.counter = Date.now()/1000;
        timer = window.setInterval(this.timer_handler,100,this);
        console.log("timer id is set",timer);
    },
    methods : {
        stopcurrenttimer() {
            console.log("stopping the timer now");
            window.clearInterval(timer);
            timer = null;
        },
        timer_handler(that) {
            let current_time = Date.now(); // in ms
            sm.update(current_time);

            this.counter = Math.floor(current_time/1000.0);
            this.phase = sm.state;

            if (sm.state=="fin") {
                window.clearInterval(timer);
                timer = null;
            }
        }
    
    }
});