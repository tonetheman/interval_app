Vue.component("tone-presetname", {
    props : ["defval"],
    data : function() {
        return {
            preset_name : this.defval
        };
    },
    methods : {
        take_input(e) {
            // keep up with the instance value
            this.preset_name = e.target.value;
            // tell your parent
            this.$emit("presetchanged",this.preset_name);
        }
    },
    template : `
    <div>
    preset name
    <input v-bind:value="preset_name"
        v-on:input="take_input"
        placeholder="preset name here..." width="40">
    </div>
    `
});
