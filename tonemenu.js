Vue.component("tone-menu", {
    methods : {
        menuClick() {
            console.log("menu click called");
        }
    },
    template : `
    <img src="ellipsis-v.svg" width="32" height="32" v-on:click="menuClick()">
    `
});
