            // EDITOR
            Vue.component("tone-editor",{
                methods : {
                    handlePresetChanged(e) {
                        this.presetName = e;
                    },
                    handleDataChange(e) {
                        if (e.title=="sets") {
                            this.setsCount = e.value;
                        } else if (e.title=="warmup") {
                            this.warmupCount = e.value;
                        } else if (e.title=="work") {
                            this.workCount = e.value;
                        } else if (e.title=="rest") {
                            this.restCount = e.value;
                        } else if (e.title=="cooldown") {
                            this.cooldownCount = e.value;
                        }
                    },
                    saveAll() {
                        console.log("save all called on editor");
                        console.log(this);
                        //localforage.length().then(function(e) {
                        //    console.log("len from local forage is",e);
                        //});
                        if (this.presetName=="") {
                            console.log("preset name cannot be blank");
                            return;
                        }

                        // now write to local storage
                        localforage.setItem(this.presetName, {
                            sets : this.setsCount,
                            warmup : this.warmupCount,
                            work : this.workCount,
                            rest : this.restCount,
                            cooldown : this.cooldownCount
                        }, function() {
                            console.log("wrote!")
                        });
                    }
                },
                data : function() {
                    return {
                        presetName : "",
                        warmupCount : 10,
                        setsCount : 11,
                        workCount : 12,
                        restCount : 13,
                        cooldownCount : 14
                    };
                },
                /*
                look at the template below
                the v-on:tonechanged on the child components
                they call up to the method on the parent handleDataChanged
                not at ALL what I would think. not intuitive at all
                */
                template : `
<div id="editor">

    <div id="menu">
        <span>
            <tone-save v-on:tonesavedata="saveAll"></tone-save>
            <tone-menu></tone-menu>
        </span>
    </div>

    <tone-presetname 
        v-bind:defval=presetName
        v-on:presetchanged="handlePresetChanged"></tone-presetname>

    <div id="maincontent">
        <tone-counter id="counter1" title="warmup" 
            v-bind:defval=warmupCount 
            v-on:tonechanged="handleDataChange"></tone-counter>
        <tone-counter id="counter2" title="sets" 
            v-bind:defval=setsCount
            v-on:tonechanged="handleDataChange"></tone-counter>
        <tone-counter id="counter3" title="work" 
            v-bind:defval=workCount
            v-on:tonechanged="handleDataChange"></tone-counter>
        <tone-counter id="counter4" title="rest" 
            v-bind:defval=restCount
            v-on:tonechanged="handleDataChange"></tone-counter>
        <tone-counter id="counter5" title="cooldown" 
            v-bind:defval=cooldownCount
            v-on:tonechanged="handleDataChange"></tone-counter>
    </div>

</div>
                `
            });
